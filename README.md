# nCounter

How long has it been since the last nCounter? Or how long until the next? Enter the event date and name, then press 'Track' to find out how long. Details appear in the user metrics on the lock screen.

Only one item is trackable. Stay focused one day at a time.

Other functions:
* Tap the in-app image to change quotes
* App will update the metric on open or change and every hour automatically if app is in focus. In conjuction with [UT Tweak Tool](https://open-store.io/app/ut-tweak-tool.sverzegnassi) preventing app suspension will allow automatic metric updates. Otherwise the metric will remind you to check the app.


## Changes
V1.2 (2020-01-20)
* NOTE: "Previous" and "Restarts" REMOVED.
(If you want that feature see: https://gitlab.com/joboticon/ncounter_old)
* Thank you to @potatonick for the suggestions
* Add ability to count down to future events
* Disallow empty events
* Easy reset button
* Save event date in the picker ("Set Today" button instead)
* Dialogues for user feedback
* French translations by @Anne17


v1.1 (2019-01-26)
* Remove timer on quote to reduce power usage; change with tap
* Metric update timer change to hourly to reduce power usage if running in background
* Minor bug fixes


## How to build

Set up [clickable](https://github.com/bhdouglass/clickable) and run `clickable` inside the working directory.

## Credits

JavaScript base on script by [Chris Hallberg](https://javascriptsource.com/days-since/)

Thanks to the help of the [Ubuntu Touch App Dev group](https://t.me/UbuntuAppDevEN), @bhdouglass and @wayneoutthere.

## To Do / Need Help

* Improve design
* Remove "restarts"? 
* Ability to track more events? Select metric for lock screen.
* Add polld to update metric outside of application

## Legal

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
